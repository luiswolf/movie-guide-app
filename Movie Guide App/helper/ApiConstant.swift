//
//  ApiConstant.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 20/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation

struct ApiConstant {
    struct Fonts {
        static let reading = "SFProText-Regular"
        static let readingBold = "SFProText-Semibold"
        static let display = "SFProDisplay-Regular"
    }
}


//
//  EndpointHelper.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation

enum ApiEndpoint: String {
    case movieDetail = "MOVIE/DETAILS"
    case movieImages = "MOVIE/IMAGES"
    case movieReviews = "MOVIE/REVIEWS"
    case discoverMovies = "DISCOVER/MOVIES"
}
class ApiHelper {
    fileprivate static let configurationFile = "ApiConstant"
    fileprivate static let endpoints = "API_ENDPOINTS"
    fileprivate static let version = "API_VERSION"
    fileprivate static let key = "API_KEY"
    fileprivate static let url = "API_URL"
    fileprivate static let imageUrl = "IMAGE_URL"
    fileprivate static let backdropUrl = "BACKDROP_URL"
    fileprivate static let endpointSeparator = "/"
    static func get(endpoint: ApiEndpoint) -> String? {
        guard let mainDict = self.get(dictForConfigurationFile: self.configurationFile) else { return nil }
        guard let url = mainDict[self.url] as? String else { return nil }
        guard let version = mainDict[self.version] as? String else { return nil }
        guard var dict = self.getEndpointDict() else { return nil }
        
        let array = endpoint.rawValue.components(separatedBy: self.endpointSeparator)
        
        var i = 0
        var endpointx = url.replacingOccurrences(of: "{version}", with: version)
        while i < array.count {
            if let str = dict[array[i]] as? String {
                endpointx.append(str)
            } else if let d = dict[array[i]] as? NSDictionary {
                dict = d
            }
            i += 1
        }
        
        if let key = mainDict[self.key] {
            endpointx.append("&api_key=\(key)")
        }
        return endpointx
    }
    private static func get(dictForConfigurationFile configurationFile: String) -> NSDictionary? {
        guard
            let path = Bundle.main.path(forResource: configurationFile, ofType: "plist"),
            let dict = NSDictionary(contentsOfFile: path)
            else {
                return nil
        }
        return dict
    }
    private static func getEndpointDict() -> NSDictionary? {
        guard let mainDict = get(dictForConfigurationFile: self.configurationFile) else { return nil }
        return mainDict[self.endpoints] as? NSDictionary
    }
    static func getImageUrl() -> String? {
        guard let mainDict = get(dictForConfigurationFile: self.configurationFile) else { return nil }
        return mainDict[self.imageUrl] as? String
    }
    static func getBackdropUrl() -> String? {
        guard let mainDict = get(dictForConfigurationFile: self.configurationFile) else { return nil }
        return mainDict[self.backdropUrl] as? String
    }
}

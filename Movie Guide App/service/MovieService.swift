//
//  MovieService.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation
import Alamofire

class MovieService {
    
    var page: Int = 0
    
    func getMovieList(callback: @escaping (RestResponse) -> Void) {
        if let endpoint = ApiHelper.get(endpoint: .discoverMovies) {
            let pagination = "&page=\(self.page)"
            Alamofire.request(endpoint + pagination, method: .get)
                .validate()
                .responseJSON {  response in

                let statusCode = response.response?.statusCode
                switch response.result {
                case .success(let data):
                    let responseObj = RestResponse(success: true, data: data, error: nil, statusCode: statusCode)

                    callback(responseObj)
                case .failure(let error):
                    let responseObj = RestResponse(success: false, data: nil, error: error, statusCode: statusCode)
                    callback(responseObj)
                }

            }
        }
    }
    
    func getMovieDetail(forId id: Int, callback: @escaping (RestResponse) -> Void) {
        if let endpoint = ApiHelper.get(endpoint: .movieDetail) {
            Alamofire.request(endpoint.replacingOccurrences(of: "{id}", with: String(id)), method: .get)
                .validate()
                .responseJSON {  response in
                    
                let statusCode = response.response?.statusCode
                switch response.result {
                case .success(let data):
                    let responseObj = RestResponse(success: true, data: data, error: nil, statusCode: statusCode)
                    
                    callback(responseObj)
                case .failure(let error):
                    let responseObj = RestResponse(success: false, data: nil, error: error, statusCode: statusCode)
                    callback(responseObj)
                }
            }
        }
    }
    
}

//
//  Movie.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 19/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation

struct Movie {
    var id: Int
    var title: String
    var originalTitle: String
    var releaseDate: Date
    var overview: String
    var backdropPath: String
    var posterPath: String
    var voteAverage: Double
    var voteCount: Int
    var genres: [String]
}

//
//  PokemonItem.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation

struct MovieItem {
    var id: Int
    var title: String
    var releaseDate: Date
}

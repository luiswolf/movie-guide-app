//
//  RestResponse.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation

struct RestResponse {
    let success: Bool
    let data: Any?
    let error: Error?
    let statusCode: Int?
}

//
//  CharacterListViewModel.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import Foundation

protocol MovieListViewModelDelegate {
    func didGetData()
    func didFinishGettingData()
    func didFailGettingData()
}

class MovieListViewModel {
    
    let formatter = DateFormatter()
    var list = [MovieItem]()
    var page: Int = 0 {
        didSet {
            service.page = page
        }
    }
    
    fileprivate var service: MovieService!
    var delegate: MovieListViewModelDelegate!
    
    init(service: MovieService = MovieService()) {
        self.service = service
    }
    
    func getList() {
        formatter.dateFormat = "YYYY-mm-DD"
        service.getMovieList { (response) in
            if response.success {
                if let data = response.data as? Dictionary<String, Any> {
                    if let results = data["results"] as? [Dictionary<String, Any>], results.count > 0 {
                        for result in results {
                            self.list.append(MovieItem(id: result["id"] as! Int, title: result["title"] as! String, releaseDate: self.formatter.date(from: result["release_date"] as! String)!))
                        }
                        self.delegate.didGetData()
                    } else {
                        self.delegate.didFinishGettingData()
                    }
                }
            } else {
                self.delegate.didFailGettingData()
            }
        }
    }
    
}

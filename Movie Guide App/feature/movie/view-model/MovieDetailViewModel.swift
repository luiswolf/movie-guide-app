//
//  MovieDetailViewModel.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 19/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

protocol MovieDetailViewModelDelegate {
    func didGetData()
    func didFailGettingData()
}

class MovieDetailViewModel {
    
    let formatter = DateFormatter()
    var movie: Movie?
    let backdropPath = ApiHelper.getBackdropUrl()
    let imagePath = ApiHelper.getImageUrl()
    fileprivate var service: MovieService!
    var delegate: MovieDetailViewModelDelegate!
    
    init(service: MovieService = MovieService()) {
        self.service = service
        formatter.dateFormat = "YYYY-mm-DD"
    }
    
    func getMovieDetail(forId id: Int) {
        
        service.getMovieDetail(forId: id) { (response) in
            if response.success, let data = response.data as? Dictionary<String, Any> {
                var id = 0
                if let _id = data["id"] as? Int {
                    id = _id
                }
                var title = ""
                if let _title = data["title"] as? String {
                    title = _title
                }
                var originalTitle = ""
                if let _originalTitle = data["original_title"] as? String {
                    originalTitle = _originalTitle
                }
                var releaseDate = Date()
                if let _releaseDate = data["release_date"] as? String, let date = self.formatter.date(from: _releaseDate) {
                    releaseDate = date
                }
                var overview = ""
                if let _overview = data["overview"] as? String {
                    overview = _overview
                }
                var backdropPath = ""
                if let _backdropPath = data["backdrop_path"] as? String {
                    backdropPath = _backdropPath
                }
                var posterPath = ""
                if let _posterPath = data["poster_path"] as? String {
                    posterPath = _posterPath
                }
                var voteAverage = 0.0
                if let _voteAverage = data["vote_average"] as? Double {
                    voteAverage = _voteAverage
                }
                var voteCount = 0
                if let _voteCount = data["vote_count"] as? Int {
                    voteCount = _voteCount
                }
                var genres = [String]()
                if let _genres = data["genres"] as? [Dictionary<String, Any>] {
                    for genre in _genres {
                        if let g = genre["name"] as? String {
                            genres.append(g)
                        }
                    }
                }
                
                self.movie = Movie(id: id, title: title, originalTitle: originalTitle, releaseDate: releaseDate, overview: overview, backdropPath: backdropPath, posterPath: posterPath, voteAverage: voteAverage, voteCount: voteCount, genres: genres)
                
                self.delegate.didGetData()
                
            } else {
                self.delegate.didFailGettingData()
            }
        }
    }
    
}

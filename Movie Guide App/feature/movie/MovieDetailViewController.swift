//
//  DetailViewController.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

class MovieDetailViewController: LWViewController {

    var detailItem: MovieItem!
    let viewModel = MovieDetailViewModel()
    var colors: UIImageColors?

    @IBOutlet weak var customBackButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var imageAreaView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    private lazy var detailViewController: MovieDetailTableViewController = {
        let viewController = MovieDetailTableViewController.prepareViewController()
        if let movie = viewModel.movie, let colors = colors {
            viewController.colors = colors
            viewController.movie = movie
        }
        viewController.delegate = self
        self.add(asChildViewController: viewController)
        return viewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        startLoading()
        if let d = detailItem {
            viewModel.getMovieDetail(forId: d.id)
        }
        UINavigationController.attemptRotationToDeviceOrientation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureOrientation()
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        configureOrientation()
    }
}

// - MARK: UIScrollView Delegate
extension MovieDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        configureNavigation(isHidden: scrollView.contentOffset.y > 0)
    }
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if scrollView.contentOffset.y <= 0 {
            configureNavigation(isHidden: false)
        }
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y <= 0 {
            configureNavigation(isHidden: false)
        }
    }
}

// - MARK: TableViewHeight Delegate
extension MovieDetailViewController: TableViewHeightDelegate {
    func didConfigureHeight(withHeight height: CGFloat) {
        containerViewHeightConstraint.constant = height
    }
}

// - MARL: UI Helper
extension MovieDetailViewController {
    func configureOrientation() {
        if UIDevice.current.userInterfaceIdiom != .pad {
            let isLandscape = DeviceHelper.isLandscape
            if !isLandscape {
                scrollView.delegate = self
            } else {
                scrollView.delegate = nil
            }
            configureNavigation(isHidden: isLandscape)
//            navigationController?.setNavigationBarHidden(isLandscape, animated: true)
//            scrollView.isScrollEnabled = !isLandscape
        } else {
            navigationController?.setNavigationBarHidden(true, animated: true)
        }
    }
    func configureView() {
        imageAreaView.clipsToBounds = false
        imageAreaView.layer.shadowColor = UIColor.black.cgColor
        imageAreaView.layer.shadowOpacity = 1
        imageAreaView.layer.shadowOffset = CGSize.zero
        imageAreaView.layer.shadowRadius = 5
        imageAreaView.backgroundColor = .clear
        imageAreaView.layer.shadowPath = UIBezierPath(roundedRect: imageAreaView.bounds, cornerRadius: 5).cgPath
        updateImages()
    }
    func configureDetail() {
        self.add(asChildViewController: detailViewController)
    }
    func configureNavigation(isHidden hidden: Bool) {
        navigationController?.setNavigationBarHidden(hidden, animated: true)
        customBackButton.isHidden = !hidden
    }
    func updateImages() {
        if let movie = viewModel.movie {
            if let imagePath = viewModel.backdropPath {
                if let url = URL(string: imagePath + movie.backdropPath) {
                    let request = NSMutableURLRequest(url: url)
                    let task = URLSession.shared.dataTask(with: request as URLRequest) {
                        data, response, error in
                        guard error == nil else { return }
                        if let data = data  {
                            DispatchQueue.main.async {
                                if let image = UIImage(data: data) {
                                    self.backgroundImageView.image = image
                                    
                                }
                            }
                        }
                    }
                    task.resume()
                }
            }
            if let imagePath = viewModel.imagePath {
                if let url = URL(string: imagePath + movie.posterPath) {
                    let request = NSMutableURLRequest(url: url)
                    let task = URLSession.shared.dataTask(with: request as URLRequest) {
                        data, response, error in
                        guard error == nil else { return }
                        if let data = data  {
                            DispatchQueue.main.async {
                                if let image = UIImage(data: data) {
                                    let myImage = UIImageView(frame: self.imageAreaView.bounds)
                                    myImage.clipsToBounds = true
                                    myImage.layer.cornerRadius = 5
                                    myImage.layer.masksToBounds = true
                                    myImage.image = image
                                    myImage.contentMode = .scaleAspectFill
                                    self.imageAreaView.addSubview(myImage)
                                    self.imageAreaView.sendSubview(toBack: self.backgroundImageView)
                                    let colors = image.getColors()
                                    self.colors = colors
                                    self.configureDetail()
                                    self.stopLoading()
                                    self.customBackButton.tintColor = colors.primary
                                    UIView.animate(withDuration: 0.3, animations: {
                                        self.view.backgroundColor = colors.background
                                        self.imageAreaView.isHidden = false
                                    })
                                }
                            }
                        }
                    }
                    task.resume()
                }
            }
        }
    }
}

// - MARK: MovieDetailViewModel Delegate
extension MovieDetailViewController : MovieDetailViewModelDelegate {
    func didGetData() {
        configureView()
    }
    func didFailGettingData() {
        stopLoading()
    }
}

// - MARK: UI Helper
extension MovieDetailViewController {
    private func add(asChildViewController viewController: UIViewController) {
        addChildViewController(viewController)
        containerView.addSubview(viewController.view)
        viewController.view.frame = containerView.bounds
        viewController.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        viewController.didMove(toParentViewController: self)
    }
}

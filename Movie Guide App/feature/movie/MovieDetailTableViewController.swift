//
//  MovieDetailTableViewController.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 20/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

protocol TableViewHeightDelegate {
    func didConfigureHeight(withHeight height: CGFloat)
}

class MovieDetailTableViewController: UITableViewController {

    var colors: UIImageColors!
    var movie: Movie!
    var delegate: TableViewHeightDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var originalTitleLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    @IBOutlet weak var overviewLabel: UILabel!
    
    let formatter = DateFormatter()
    let fontBold = UIFont(name: ApiConstant.Fonts.readingBold, size: 14.0)
    let fontRegular = UIFont(name: ApiConstant.Fonts.reading, size: 14.0)
    static let storyboardName: String = "Movie"
    static func prepareViewController() -> MovieDetailTableViewController {
        let storyboard = UIStoryboard(name: self.storyboardName, bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MovieDetailTableViewController") as! MovieDetailTableViewController
        return vc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 100.0
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.backgroundColor = .clear
        tableView.separatorColor = colors.background.lighter() == .white ? colors.primary.withAlphaComponent(0.2) : colors.background
        tableView.cellLayoutMarginsFollowReadableWidth = false
        configure()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        delegate?.didConfigureHeight(withHeight: tableView.contentSize.height)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        delegate?.didConfigureHeight(withHeight: tableView.contentSize.height)
    }
}

// - MARK: UI Helper
extension MovieDetailTableViewController {
    func configure() {
        // MARK: TITLE
        titleLabel.text = movie.title
        originalTitleLabel.text = movie.originalTitle
        
        // MARK: GENRE
        let genre = NSMutableAttributedString(
            string: "Gênero: ",
            attributes: [.font : fontBold!]
        )
        genre.append(
            NSAttributedString(
                string: movie.genres.joined(separator: ", "),
                attributes: [.font : fontRegular!]
            )
        )
        genresLabel.attributedText = genre
        
        // MARK: RELEASE DATE
        formatter.dateFormat = "DD/mm/YYYY"
        let releaseDate = NSMutableAttributedString(
            string: "Lançamento: ",
            attributes: [.font : fontBold!]
        )
        releaseDate.append(
            NSAttributedString(
                string: formatter.string(from: movie.releaseDate),
                attributes: [.font : fontRegular!]
            )
        )
        releaseDateLabel.attributedText = releaseDate
        
        // MARK: RATING
        let rate = NSMutableAttributedString(
            string: "Avaliação: ",
            attributes: [.font : fontBold!]
        )
        rate.append(
            NSAttributedString(
                string: "\(movie.voteAverage) (\(movie.voteCount) votos)",
                attributes: [.font : fontRegular!]
            )
        )
        rateLabel.attributedText = rate
        
        // MARK: OVERVIEW
        overviewLabel.text = movie.overview
    }
}

// - MARK: TableView
extension MovieDetailTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        if movie.overview.isEmpty { return 1 }
        return super.numberOfSections(in: tableView)
    }
    override func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let header = view as? UITableViewHeaderFooterView else { return }
        header.textLabel?.textColor = colors.primary
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        cell.contentView.backgroundColor = colors.background.lighter(by: 80.0).withAlphaComponent(0.3)
        if cell.responds(to: #selector(setter: UITableViewCell.separatorInset)) {
            cell.separatorInset = UIEdgeInsets.zero
        }
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
        cell.backgroundColor = UIColor.white
        cell.textLabel?.backgroundColor = .clear
        cell.detailTextLabel?.backgroundColor = .clear
        return cell
    }
}

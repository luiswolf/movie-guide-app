//
//  MasterViewController.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

class MovieListViewController: LWTableViewController {

    let formatter = DateFormatter()
    let viewModel = MovieListViewModel()
    
    var detailViewController: MovieDetailViewController? = nil
    var canGetData: Bool = true
    var isFirstLoad: Bool = true
    
    fileprivate let goToDetailIdentifier = "showDetail"
    fileprivate let cellIdentifier = "Cell"
    fileprivate let loadingCellIdentifier = "loadingCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let split = splitViewController {
            split.preferredDisplayMode = .allVisible
            let controllers = split.viewControllers
            detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? MovieDetailViewController
        }

        formatter.dateFormat = "DD/mm/YYYY"
        tableView.register(UINib(nibName: "LoadingTableViewCell", bundle: nil), forCellReuseIdentifier: loadingCellIdentifier)
        viewModel.delegate = self
        
        startLoading()
        getData()
    }

    override func viewWillAppear(_ animated: Bool) {
        clearsSelectionOnViewWillAppear = splitViewController!.isCollapsed
        navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillAppear(animated)
    }
    
    func getData() {
        viewModel.page += 1
        tableView.reloadSections(IndexSet(integer: 1), with: .none)
        viewModel.getList()
        canGetData = false
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == goToDetailIdentifier, let indexPath =
            tableView.indexPathForSelectedRow {
            let object = viewModel.list[indexPath.row]
            if let navigationController = segue.destination as? UINavigationController, let vc = navigationController.topViewController as? MovieDetailViewController {
                vc.title = object.title
                vc.detailItem = object
                vc.navigationItem.leftBarButtonItem = splitViewController?.displayModeButtonItem
                vc.navigationItem.leftItemsSupplementBackButton = true
            }
        }
    }
    
    @IBAction func unwindToMovieListViewController(segue : UIStoryboardSegue) {}
}

// - MARK: TableView
extension MovieListViewController {
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "LANÇAMENTOS DE 2018"
        }
        return nil
    }
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return .leastNonzeroMagnitude
        }
        return UITableViewAutomaticDimension
    }
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNonzeroMagnitude
    }
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return viewModel.list.count
        } else if section == 1 && canGetData {
            return 1
        }
        return 0
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
            let object = viewModel.list[indexPath.row]
            cell.textLabel?.text = object.title
            cell.detailTextLabel?.text = formatter.string(from: object.releaseDate)
            cell.accessoryType = .disclosureIndicator
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: loadingCellIdentifier, for: indexPath) as! LoadingTableViewCell
            cell.activityIndicator.startAnimating()
            cell.accessoryType = .none
            return cell
        }
    }
}

// - MARK: ScrollView
extension MovieListViewController {
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offsetY = scrollView.contentOffset.y
        let contentHeight = scrollView.contentSize.height
        if canGetData && offsetY > contentHeight - scrollView.frame.height * 4 {
            getData()
        }
    }
}

// - MARK: CharacterListViewModel Delegate
extension MovieListViewController: MovieListViewModelDelegate {
    func didGetData() {
        canGetData = true
        tableView.reloadData()
        if UIDevice.current.userInterfaceIdiom == .pad, isFirstLoad {
            isFirstLoad = false
           tableView.selectRow(at: IndexPath(row: 0, section: 0), animated: true, scrollPosition: .none)
            self.performSegue(withIdentifier: goToDetailIdentifier, sender: nil)
        }
        stopLoading()
    }
    func didFinishGettingData() {
        canGetData = false
        tableView.reloadData()
    }
    func didFailGettingData() {
        stopLoading()
        canGetData = true
    }
}


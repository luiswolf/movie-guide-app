//
//  UIColor.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 19/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

extension UIColor {
    static let darker = #colorLiteral(red: 0.2039215686, green: 0.2392156863, blue: 0.2745098039, alpha: 1)
    static let dark = #colorLiteral(red: 0.3098039216, green: 0.3568627451, blue: 0.4, alpha: 1)
    static let main = #colorLiteral(red: 0.3960784314, green: 0.4509803922, blue: 0.4941176471, alpha: 1)
    static let light = #colorLiteral(red: 0.6549019608, green: 0.6784313725, blue: 0.7294117647, alpha: 1)
    static let lighter = #colorLiteral(red: 0.7529411765, green: 0.7725490196, blue: 0.8078431373, alpha: 1)
    static let white = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
}

import UIKit

extension Comparable {
    func clamped(to range: ClosedRange<Self>) -> Self {
        if self > range.upperBound {
            return range.upperBound
        } else if self < range.lowerBound {
            return range.lowerBound
        } else {
            return self
        }
    }
}

extension UIColor {
    public func lighter(by percentage: CGFloat = 50.0) -> UIColor {
        return self.adjustBrightness(by: abs(percentage))
    }
    public func darker(by percentage: CGFloat = 50.0) -> UIColor {
        return self.adjustBrightness(by: -abs(percentage))
    }
    func adjustBrightness(by percentage: CGFloat = 50.0) -> UIColor {
        var red: CGFloat = 0.0
        var green: CGFloat = 0.0
        var blue: CGFloat = 0.0
        var alpha: CGFloat = 0.0
        
        if self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) {
            let pFactor = (100.0 + percentage) / 100.0
            
            let newRed = (red * pFactor).clamped(to: 0.0 ... 1.0)
            let newGreen = (green * pFactor).clamped(to: 0.0 ... 1.0)
            let newBlue = (blue * pFactor).clamped(to: 0.0 ... 1.0)
            
            return UIColor(red: newRed, green: newGreen, blue: newBlue, alpha: alpha)
        }
        
        return self
    }
}

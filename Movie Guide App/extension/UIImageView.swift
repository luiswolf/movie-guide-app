//
//  UIImage.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 19/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

extension UIImageView {
    func create(fromUrlString urlString: String) {
        if let url = URL(string: urlString) {
            let request = NSMutableURLRequest(url: url)
            let task = URLSession.shared.dataTask(with: request as URLRequest) {
                data, response, error in
                guard error == nil else { return }
                if let data = data  {
                    DispatchQueue.main.async {
                        self.image = UIImage(data: data)
                    }
                }
            }
            task.resume()
        }
    }
}

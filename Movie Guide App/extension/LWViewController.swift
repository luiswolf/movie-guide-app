//
//  LWViewController.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 19/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

class LWViewController: UIViewController{
    
    var activityIndicator = UIActivityIndicatorView()
    
    lazy var loaderView: UIView = {
        let loader = UIView()
        loader.layer.zPosition = 998
        loader.frame = view.bounds
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.backgroundColor = .white
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.hidesWhenStopped = true
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.activityIndicatorViewStyle = .gray
        loader.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        return loader
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.isTranslucent = true
        automaticallyAdjustsScrollViewInsets = false
    }
    
    final func startLoading() {
        view.addSubview(loaderView)
        setConstraints(forView: loaderView)
    }
    
    final func stopLoading() {
        loaderView.removeFromSuperview()
    }
    
    private func setConstraints(forView v: UIView) {
        if #available(iOS 11.0, *) {
            v.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0.0).isActive = true
            v.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            v.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
            v.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0.0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: 0).isActive = true
        }
        v.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerX, relatedBy: NSLayoutRelation.equal, toItem: v, attribute: NSLayoutAttribute.centerX, multiplier: 1, constant: 0))
        v.addConstraint(NSLayoutConstraint(item: activityIndicator, attribute: NSLayoutAttribute.centerY, relatedBy: NSLayoutRelation.equal, toItem: v, attribute: NSLayoutAttribute.centerY, multiplier: 1, constant: 0))
    }

}

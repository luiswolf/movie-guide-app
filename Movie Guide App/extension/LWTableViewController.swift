//
//  LWTableViewController.swift
//  Movie Guide App
//
//  Created by Luís Wolf on 17/09/2018.
//  Copyright © 2018 Luís Wolf. All rights reserved.
//

import UIKit

class LWTableViewController: UITableViewController {

    var activityIndicator = UIActivityIndicatorView()
    
    lazy var loaderView: UIView = {
        let loader = UIView()
        loader.layer.zPosition = 998
        loader.frame = view.bounds
        loader.translatesAutoresizingMaskIntoConstraints = false
        loader.backgroundColor = .white
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.hidesWhenStopped = true
        activityIndicator.center = loader.center
        activityIndicator.activityIndicatorViewStyle = .gray
        loader.addSubview(activityIndicator)
        
        activityIndicator.startAnimating()
        
        return loader
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    final func startLoading() {
        tableView.setContentOffset(.zero, animated: false)
        tableView.isScrollEnabled = false
        view.addSubview(loaderView)
        setConstraints(forView: loaderView)
    }
    
    final func stopLoading() {
        tableView.isScrollEnabled = true
        loaderView.removeFromSuperview()
    }
    
    private func setConstraints(forView v: UIView) {
        if #available(iOS 11.0, *) {
            v.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leftAnchor, constant: 0.0).isActive = true
            v.rightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.rightAnchor, constant: 0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        } else {
            v.topAnchor.constraint(equalTo: view.layoutMarginsGuide.topAnchor, constant: 0.0).isActive = true
            v.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0.0).isActive = true
            v.widthAnchor.constraint(equalTo: view.widthAnchor, constant: 0.0).isActive = true
            v.bottomAnchor.constraint(equalTo: view.layoutMarginsGuide.bottomAnchor, constant: 0).isActive = true
        }
    }

}

# Movie Guide App

Desafio para a vaga de Desenvolvedor iOS.

> Criar um aplicativo que utilize a API pública da Marvel: https://developer.marvel.com/.
> Esse aplicativo deverá listar os personagens em uma tela com rolagem infinita e apresentar os detalhes do personagem em uma tela auxiliar com suas devidas participações em Comics (informação disponível na API).
> 
> Lembre-se que você será avaliado não apenas em relação ao código, mas também em
> questões como: UI, UX e organização do projeto.

Como a API da marvel encontrava-se indisponível durante o desenvolvimento deste desafio, foi utilizada a API https://developers.themoviedb.org (versão 3).

O aplicativo foi desenvolvido para atender basicamento os requisitos do desafio e foi dedicado mais tempo na tela de Detalhe do filme, onde apliquei alguns comportamentos de navegação e tratamento de tela para todos dispositivos/rotações possíveis.

Escolhi como versão mínima de SO o iOS 9 pois verifiquei que o aplicativo da empresa também possui esta configuracão.

Para start do projeto, escolhi o template do XCode Master-Detail App, pois o mesmo já apresenta uma estrutura de navegação de acordo com o que foi solicitado no desafio e também traz como padrão o SplitViewController (utilizado no iPad para apresentar duas views ao mesmo tempo).

O tempo de desenvolvimento do projeto era de 72 horas, porém o mesmo foi desenvolvido em torno de 12 horas (durante o período de 18/09/2018 e 21/09/2018).

Os testes unitários não foram desenvolvidos por falta de tempo, pois atualmente estou trabalhando e o projeto foi desenvolvido no meu tempo livre.

## Requisitos

* iOS 9.0+ / macOS 10.10+ / tvOS 9.0+ / watchOS 2.0+
* Xcode 9.4.1+
* Swift 4.1+

## Dependências

Bibliotecas (terceiras) utilizadas:

* [Alamofire] - Biblioteca para fazer requisições à API da The Movie Data Base
* [UIImageColors] - Classe que obtém as principais cores de uma imagem 

[Alamofire]: <https://github.com/Alamofire/Alamofire>
[UIImageColors]: https://github.com/jathu/UIImageColors

## Apresentação do aplicativo

### Lista de Filmes

Esta é a tela inicial do aplicativo. Nela, consta uma lista de filmes de 2018 ordenados pela popularidade (*esta escolha foi realizada apenas para atender o requisito do desafio*).

A medida que o usuário visualiza os registros mais abaixo da lista, é realizada uma consulta para obter novos registros e os mesmos são apresentados (Scroll Infinito).

<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-list.PNG" width="300">

### Detalhe do Filme

A tela de detalhamento de filmes apresenta detalhes básicos do filme (Título, título original, gênero, avaliação, data de lançamento e sinopse). 

Quando o usuário faz scroll para visualizar o conteúdo, é escondida a barra de navegação dando mais atenção para o conteúdo e é exibido um botão auxiliar para que o usuário possa retornar para a tela anterior.

<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-detail-1.PNG" width="300">
<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-detail-2.PNG" width="300">

### Detalhe do Filme - Tela em modo paisagem

Em modo paisagem (iPhone), é exibida uma interface um pouco diferente, apresentando sempre visível o poster do filme.

<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-detail-3.PNG" width="600">

### Variação de layout conforme poter do filme

Para a tela de detalhamento foi utilizada uma biblioteca/classe que obtem as principais cores de uma imagem. 
Utilizei a mesma para dar um aparência mais personalizada de acordo com cada filme escolhido.

<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-detail-4.PNG" width="300">
<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-detail-5.PNG" width="300">

### iPad

A aplicação também foi desenvolvida para iPad, onde ambas telas são apresentadas juntamente (SplitViewController).

<img src="https://gitlab.com/luiswolf/movie-guide-app/raw/master/screenshots/movie-detail-ipad-2.png" width="800">

## Checklist de atividades

- [x] Integração com API da The Movie DB
- [x] Tela com lista de items
- [x] Rolagem infinita
- [x] Tela com detalhamente do item
- [x] UI / UX
- [x] Organização do projeto
- [x] Repositório público
- [x] README
- [x] Código nativo
- [x] Arquitetura MVVM
- [ ] Testes unitários
